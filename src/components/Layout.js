import React from 'react';
import Header from './Header';
import Footer from './Footer';
import { useSiteMetadata } from '../hooks/useSiteMetadata';
import styled from 'styled-components';



const AppContainer = styled.div`
  display: flex;
  justify-content: center;
  @media (max-width: 425px) {
    margin: 0.5em;
  }
`

const Main = styled.div`
  width: 640px;
  @media (max-width: 425px) {
    width: 100%;
  }
`
const Layout = ({ children }) => {
  const site  = useSiteMetadata();
  return(
    <AppContainer>
      <Main>
        <Header 
          title={site.title} 
          description={site.description}
        />
        {children}
        <Footer/>
      </Main>
    </AppContainer>
  )
}

export default Layout;
