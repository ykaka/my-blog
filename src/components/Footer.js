import React from 'react';
import styled from 'styled-components';

const List = styled.li`
  list-style: none;
  display: inline-block;
  margin-right: 1em;
  font-weight: bold;
  font-size: 16px;
`

const Footer = () => {
  return(
    <footer style={{ textAlign: 'left', marginTop: '3rem' }}>
      <List><a href="https://github.com/Yudikrisnandi/Yudikrisnandi" rel="noreferrer" target="_blank">Github</a></List>
      <List><a href="https://www.linkedin.com/in/yudikrisnandi22/" rel="noreferrer" target="_blank">Linkedin</a></List>
    </footer>
  )
}
export default Footer;
