import React from 'react';
import Highlight, { defaultProps } from "prism-react-renderer";
import theme from 'prism-react-renderer/themes/nightOwl';
import styled from 'styled-components';
//import { copyToClipboard } from '../utils/copyToClipboard';
/*
 * add react live
import {
  LiveProvider,
  LiveEditor,
  LiveError,
  LivePreview
} from 'react-live'*/

const Pre = styled.pre`
  position: relative;
  text-align: left;
  margin: 1em 0;
  padding: 0.5em;
  border-radius: 3px;
  overflow: auto;
  & .token-line {
    line-height: 1.3em;
    height: 1.3em;
  }
`

const LineNumber = styled.span`
  display: inline-block;
  width: 2em;
  user-select: none;
  opacity: 0.3;
`

/*const CopyButton = styled.button`
  position: fixed;
  right: 0.25em;
  border: 0;
  border-radius: 3px;
  margin: 0.25em;
  opacity: 0.3;
  :hover {
    opacity: 1;
  }
`*/

const CodeBlock = ({ codeString, language, ...props }) => {
  return(
    <Highlight 
      {...defaultProps} 
      code={codeString} 
      language={language}
      theme={theme}
    >
      {({ className, style, tokens, getLineProps, getTokenProps }) => (
        <Pre className={className} style={style}>
          {tokens.map((line, i) => (
            <div {...getLineProps({ line, key: i })}>
              <LineNumber>{i + 1}</LineNumber>
              {line.map((token, key) => (
                <span {...getTokenProps({ token, key })} />
              ))}
            </div>
          ))}
        </Pre>
      )}
    </Highlight>
  )
}

export default CodeBlock;
