import React from 'react';
import { Link } from 'gatsby';
import styled from 'styled-components';
import vitae from '../assets/vitae.jpg';

const HeaderContainer = styled.div`
  display: flex;
  margin-top: 10px;
  align-items: center;
  margin-bottom: 10px;
`;

const Logo = styled.img`
  width: 100px;
  height: 100px;
  border-radius: 50%;
`;

const HeaderTitle = styled.div`
  margin-left: 10px;
`
const NavigationContainer = styled.nav`
  display: flex;
  justify-content: center;
  align-items: center;
  border-bottom: 1px solid grey;
  border-top: 1px solid grey;
  padding: 5px 0;
`

const Navigation = styled.nav`
  width: 40%;
  display: flex;
  justify-content: space-between;
`

const Title = styled.h1`
  margin: 0;
  margin-bottom: 5px;
`

const Desc = styled.p`
  margin: 0;
`


function Header() {
  return (
    <>
      <HeaderContainer>
        <Logo src={vitae} alt="logo"/>
        <HeaderTitle>
          <Title>Yudi Krisnandi</Title>
          <Desc>I'm a Programmer 👨‍💻 | Calisthenic Atlete 🔥</Desc>
        </HeaderTitle>
      </HeaderContainer>
      <NavigationContainer>
        <Navigation>
          <Link to="/">Blog</Link>
          <Link to="/course">Course</Link>
        </Navigation>
      </NavigationContainer>
    </>
  );
}

export default Header;
