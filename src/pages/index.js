import React from 'react';
import Layout from '../components/Layout';
import { graphql, Link } from 'gatsby';

export const query = graphql`
  query GET_BLOGS {
    allMdx(
      sort: {fields: [frontmatter___date]}
      filter: {frontmatter: {type: {eq: "blog"}}}
    ) {
      nodes {
        id
        excerpt(pruneLength: 250)
        frontmatter {
          date
          title
          description
        }
        fields {
          slug
        }
      }
    }
  }
`

const Home = ({ data }) => {
  return(
    <Layout>
      {data.allMdx.nodes.map(({ id, frontmatter, excerpt, fields }) => (
        <div key={id}>
          <Link to={`${fields.slug}`}>
            <h2>
              {frontmatter.title}
            </h2>
          </Link>
          <p>{frontmatter.description}</p>
        </div>
      ))}
    </Layout>
  )
}

export default Home;
