import React from 'react';
import Layout from '../components/Layout'
import styled from 'styled-components';
import react from '../assets/react-masterclass-untuk-semua.jpg';
import git from '../assets/git-masterclass-untuk-semua.png';

const CourseContainer = styled.div`
  display: flex;
  height: 120px;
  margin: 0.5em 0; 
  margin-bottom: 1em;
  @media (max-width: 425px) {
  }
`
const ImageContainer = styled.div`
  width: 30%;
`
const DescriptionContainer = styled.div`
  width: 50%;
  padding: 0 0.5em;
  h3 {
    margin: 0;
  }
  @media (max-width: 425px) {
    p {
      font-size: 14px;
    }
  }
`

const Price = styled.div`
  width: 20%;
  display: flex;
  flex-direction: column;
`

const Image = styled.img`
  width: 100%;
  height: 100%;
`

const Button = styled.a`
  padding: 5px 10px;
  background:#54A1F8;
  font-weight: bold;
  border: 0;
  border-radius: 3px;
  color: white;
  text-decoration: none;
  @media (max-width: 425px) {
    font-size: 14px;
  }
`

const data = [
  {
    id: 1,
    title: "React Masterclass Untuk Semua",
    description: "Belajar React dari dasar sampai advanced topik seperti context-api | redux | redux-saga | Ant design | styled-component | dll",
    image: react,
    link: "https://www.udemy.com/course/react-master-class-untuk-semua/?couponCode=06BA1824DDA5F9B70D0D"
  },
  {
    id: 1,
    title: "Git Masterclass Untuk Semua",
    description: "Belajar menggunakan version control yang sesuai dengan standar industri",
    image: git,
    link: "https://www.udemy.com/course/git-masterclass-untuk-semua/?couponCode=01830EA746295B38205C"
  },
]

const price = "Rp129rb"
const Course = () => {
  return(
    <Layout>
      {data.map(({ id, title, description, image, link}) => (
        <CourseContainer>
          <ImageContainer>
            <Image src={image} alt={title} />
          </ImageContainer>
          <DescriptionContainer>
            <a href={link} target="_blank" rel="noreferrer"><h3>{title}</h3></a>
            <p>{description}</p>
          </DescriptionContainer>
          <Price>
            <b style={{ marginBottom: '1em' }}>{price}</b>
            <Button href={link}i target="_blank" rel="noreferrer">Join Sekarang</Button>
          </Price>
        </CourseContainer>
      ))}
    </Layout>
  )
}

export default Course;
