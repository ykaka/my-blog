import React from 'react';
import { MDXProvider } from '@mdx-js/react';
import CodeBlock from './src/components/CodeBlock';

const components = {
  h1: ({ children }) => (
    <h1 style={{ fontSize: '48px' }}>{children}</h1>
  ),
  h2: ({ children }) => (
    <h2 style={{ fontSize: '30px' }}>{children}</h2>
  ),
  h3: ({ children }) => (
    <h3 style={{ fontSize: '21px' }}>{children}</h3>
  ),
  p: ({ children }) => (
    <p style={{ fontSize: '21px' }}>{children}</p>
  ),
  li: ({ children }) => (
    <li style={{ fontSize: '21px' }}>{children}</li>
  ),
  pre: ({ children: { props } }) => {
    if(props.mdxType === 'code') {
      return(
        <CodeBlock
          codeString={props.children.trim()}
          language={props.className && props.className.replace('language-', '')}
          {...props}
        />
      )
    }
  }
}

export const wrapRootElement = ({ element }) => (
  <MDXProvider components={components}>{element}</MDXProvider>
) 
