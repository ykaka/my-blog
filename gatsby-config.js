const siteMetadata = {
  title: `Yudi Krisnandi`,
  description: `yudi krisnandi personal blog`,
  image: `/default-site-image.jpg`,
  siteUrl: `https://yudikrisnandi.netlify.app/`,
  siteLanguage: `id`,
  siteLocale: `id`,
  twitterUsername: ``,
  authorName: `Yudi Krisnandi`,
}


module.exports = {
  siteMetadata: siteMetadata,
  plugins: [
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`, 
    `gatsby-plugin-styled-components`,
    `gatsby-remark-grid-tables`,
    `gatsby-plugin-mdx-embed`,
    {
      resolve: `gatsby-plugin-mdx`,
      options: {
        extensions: [`.mdx`, `.md`],
      },
      gatsbyRemarkPlugins: [
        {
          resolve: `gatsby-remark-images`
        }
      ],
      plugin: [
        {
          resolve: `gatsby-remark-images`
        }
      ]
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/posts`,
        name: `posts`,
      },
    },
    {
      resolve: "gatsby-transformer-remark",
      options: {
        plugins: [
          {
            resolve: `@gatsby-contrib/gatsby-remark-link-youtube`,
            options: {
              width: 768,
              className: `center`,
              title: `Cliquer pour voir la vidéo sur youtube.com`,
            }
          }
        ]
      }
    }
  ]
};
